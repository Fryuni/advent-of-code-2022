#![feature(iter_array_chunks)]
#![feature(try_blocks)]
#![forbid(unsafe_code)]
#![allow(dead_code, unused_imports)]
#![deny(
    missing_debug_implementations,
    missing_copy_implementations,
    missing_docs
)]

//! CLI for running Advent of Code solutions

use std::ffi::OsStr;
use std::fs::{FileType, ReadDir};
use std::path::{Path, PathBuf};

use clap::{Parser, ValueEnum};
use color_eyre::eyre::{bail, eyre, Context};
use color_eyre::{eyre, Result};
use either::Either;
use itertools::Itertools;
use try_block::try_block;

use aoc::ext::flat_result;

mod registry;
mod y2019;
mod y2020;
mod y2021;
mod y2022;

#[derive(Debug, Copy, Clone, ValueEnum)]
enum ChallengePart {
    One,
    Two,
    All,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short = 'a', long = "add")]
    add_data: Option<String>,

    #[arg(
        short = 'y',
        long = "year",
        value_parser = clap::value_parser!(u16).range(2019..=2022),
        default_value = "2022",
    )]
    year: u16,

    #[arg(
        short = 'd',
        long = "day",
        value_parser = clap::value_parser!(u8).range(1..=25),
        default_value = "1",
    )]
    day: u8,

    #[arg(short = 'p', long = "part", default_value = "one")]
    challenge_part: ChallengePart,

    #[arg(short = 'f', long = "file")]
    data_file: Vec<String>,
}

struct FileFinder {
    dir_source: Either<Option<PathBuf>, std::fs::ReadDir>,
}

impl Iterator for FileFinder {
    type Item = Result<(String, PathBuf)>;

    fn next(&mut self) -> Option<Self::Item> {
        let source = match &mut self.dir_source {
            Either::Left(None) => return None,
            Either::Left(Some(path)) => {
                let read_dir =
                    match std::fs::read_dir(&*path).context("reading the problem directory") {
                        Ok(read_dir) => read_dir,
                        Err(err) => return Some(Err(err)),
                    };

                self.dir_source = Either::Right(read_dir);
                self.dir_source.as_mut().right()?
            }
            Either::Right(read_dir) => read_dir,
        };

        let entry = match source.next() {
            None => return None,
            Some(Err(err)) => return Some(Err(err).context("iterating directory")),
            Some(Ok(entry)) => entry,
        };

        let path = entry.path();

        Some(
            match path.file_stem().and_then(OsStr::to_str).map(String::from) {
                None => Err(eyre!("invalid file name")),
                Some(stem) => Ok((stem, path)),
            },
        )
    }
}

impl Args {
    fn file_path(&mut self) -> impl Iterator<Item = Result<(String, PathBuf)>> {
        let base = String::from("data");
        let year = self.year.to_string();
        let day = self.day.to_string();

        if self.data_file.is_empty() {
            let folder_path = PathBuf::from_iter([&base, &year, &day]);

            return Either::Left(FileFinder {
                dir_source: Either::Left(Some(folder_path)),
            });
        }

        Either::Right(
            std::mem::take(&mut self.data_file)
                .into_iter()
                .map(move |file| {
                    let path = PathBuf::from_iter([&base, &year, &day, &file]);

                    if path.is_file() {
                        Ok((file, path))
                    } else {
                        bail!("Not a file: {file}")
                    }
                }),
        )
    }

    fn run(mut self) -> Result<()> {
        let solution = registry::get_solution(self.year, self.day);

        self.file_path()
            .map_ok(|(name, path)| -> Result<_> { Ok((name, std::fs::read_to_string(path)?)) })
            .map(|res| res?)
            .map_ok(|(name, input_data)| -> Result<()> {
                println!("Processing file {name}");

                let result = match self.challenge_part {
                    ChallengePart::One => solution.part_one(input_data),
                    ChallengePart::Two => solution.part_two(input_data),
                    ChallengePart::All => {
                        println!(
                            "The solution for part one is: {}",
                            solution.part_one(input_data.clone())?
                        );
                        println!(
                            "The solution for part two is: {}",
                            solution.part_two(input_data)?
                        );

                        return Ok(());
                    }
                }?;

                println!("The solution is: {result}");

                Ok(())
            })
            .map(flat_result)
            .collect_vec()
            .into_iter()
            .collect()
    }
}

fn main() -> Result<()> {
    {
        color_eyre::install()?;
        let mut builder = pretty_env_logger::formatted_builder();

        if let Ok(val) = std::env::var("RUST_LOG") {
            builder.parse_filters(&val);
        } else {
            builder.filter(None, log::LevelFilter::Info);
        }

        builder.try_init()?;
    }

    let args: Args = Parser::parse();

    args.run()
}
