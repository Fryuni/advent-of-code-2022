//! Library of utility functions, algorithms and data structures.

#![feature(iter_array_chunks)]
#![deny(unsafe_code)]
// #![deny(
//     missing_debug_implementations,
//     missing_copy_implementations,
//     missing_docs
// )]

pub mod ext;
pub mod grid;
pub mod observer;
pub mod parse;
