use either::Either;
use std::collections::{BTreeMap, HashMap};
use std::iter::FusedIterator;
use std::ops::RangeInclusive;

pub trait Grid {
    type Item;

    fn bounds(&self) -> GridBounds;

    fn get(&self, pos: (usize, usize)) -> Option<&Self::Item>;

    fn get_mut(&mut self, pos: (usize, usize)) -> Option<&mut Self::Item>;

    fn size(&self) -> (usize, usize) {
        let GridBounds { x, y } = self.bounds();

        (x.size(), y.size())
    }

    fn spiral(&self) -> GridAccessIter<'_, Self, SpiralIter> {
        let size = self.size();
        GridAccessIter {
            iter: SpiralIter::from_grid_layout(size.0, size.1),
            grid: self,
        }
    }

    fn spiral_mut(&mut self) -> GridAccessIterMut<'_, Self, SpiralIter> {
        let size = self.size();
        GridAccessIterMut {
            iter: SpiralIter::from_grid_layout(size.0, size.1),
            grid: self,
        }
    }
}

pub struct GridAccessIter<'a, G: ?Sized, I> {
    iter: I,
    grid: &'a G,
}

impl<'a, G, I> Iterator for GridAccessIter<'a, G, I>
where
    G: Grid + ?Sized,
    I: Iterator<Item = (usize, usize)>,
{
    type Item = ((usize, usize), &'a G::Item);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter
            .next()
            .and_then(|pos| Some((pos, self.grid.get(pos)?)))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}

impl<'a, G, I> DoubleEndedIterator for GridAccessIter<'a, G, I>
where
    G: Grid + ?Sized,
    I: DoubleEndedIterator<Item = (usize, usize)>,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.iter
            .next_back()
            .and_then(|pos| Some((pos, self.grid.get(pos)?)))
    }
}
pub struct GridAccessIterMut<'a, G: ?Sized, I> {
    iter: I,
    grid: &'a mut G,
}

impl<'a, G, I> Iterator for GridAccessIterMut<'a, G, I>
where
    G: Grid + ?Sized,
    I: Iterator<Item = (usize, usize)>,
{
    type Item = ((usize, usize), &'a mut G::Item);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().and_then(|pos| {
            Some((pos, {
                let item = self.grid.get_mut(pos)?;

                #[allow(unsafe_code)]
                // SAFETY: The constructed iterators yield non-repeated positions,
                //         so no overlapping mutable borrows happen.
                unsafe {
                    &mut *(item as *mut _)
                }
            }))
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}

impl<'a, G, I> DoubleEndedIterator for GridAccessIterMut<'a, G, I>
where
    G: Grid + ?Sized,
    I: DoubleEndedIterator<Item = (usize, usize)>,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.iter.next_back().and_then(|pos| {
            Some((pos, {
                let item = self.grid.get_mut(pos)?;

                #[allow(unsafe_code)]
                // SAFETY: The constructed iterators yield non-repeated positions,
                //         so no overlapping mutable borrows happen.
                unsafe {
                    &mut *(item as *mut _)
                }
            }))
        })
    }
}

macro_rules! iter_marker {
    ($name:ident) => {
        impl<'a, G, I> $name for GridAccessIter<'a, G, I>
        where
            Self: Iterator,
            I: $name,
        {
        }

        impl<'a, G, I> $name for GridAccessIterMut<'a, G, I>
        where
            Self: Iterator,
            I: $name,
        {
        }
    };
}

iter_marker!(FusedIterator);
iter_marker!(ExactSizeIterator);

#[derive(Debug, Copy, Clone)]
pub struct Bound {
    min: usize,
    max: usize,
}

#[derive(Debug, Copy, Clone)]
pub struct GridBounds {
    x: Bound,
    y: Bound,
}

impl Bound {
    pub fn new(a: usize, b: usize) -> Self {
        Self {
            min: usize::min(a, b),
            max: usize::max(a, b),
        }
    }

    pub fn min(&self) -> usize {
        self.min
    }

    pub fn max(&self) -> usize {
        self.max
    }

    pub fn size(&self) -> usize {
        self.max - self.min
    }

    pub fn iter(&self) -> RangeInclusive<usize> {
        self.min..=self.max
    }

    pub fn neighbors(self, val: usize) -> Option<Either<usize, [usize; 2]>> {
        if self.min == self.max {
            None
        } else if val == self.min {
            Some(Either::Left(val + 1))
        } else if val == self.max {
            Some(Either::Left(val - 1))
        } else {
            Some(Either::Right([val - 1, val + 1]))
        }
    }
}

impl GridBounds {
    pub fn iter_rows(&self) -> RowsIter {
        RowsIter {
            rows: self.x.iter(),
            column_bounds: self.y,
        }
    }

    pub fn iter_columns(&self) -> ColumnsIter {
        ColumnsIter {
            columns: self.y.iter(),
            row_bounds: self.x,
        }
    }

    pub fn neighbors(self, val: (usize, usize)) -> Vec<(usize, usize)> {
        let (vx, vy) = val;

        match (self.x.neighbors(vx), self.y.neighbors(vy)) {
            (None, None) => Vec::new(),
            (Some(Either::Left(x)), None) => vec![(x, vy)],
            (None, Some(Either::Left(y))) => vec![(vx, y)],
            (Some(Either::Left(x)), Some(Either::Left(y))) => vec![(vx, y), (x, vy), (x, y)],
            (Some(Either::Right([x1, x2])), None) => vec![(x1, vy), (x2, vy)],
            (None, Some(Either::Right([y1, y2]))) => vec![(vx, y1), (vx, y2)],
            (Some(Either::Left(x)), Some(Either::Right([y1, y2]))) => {
                vec![(x, vy), (vx, y1), (vx, y2), (x, y1), (x, y2)]
            }
            (Some(Either::Right([x1, x2])), Some(Either::Left(y))) => {
                vec![(vx, y), (x1, vy), (x2, vy), (x1, y), (x2, y)]
            }
            (Some(Either::Right([x1, x2])), Some(Either::Right([y1, y2]))) => vec![
                (x1, y1),
                (vx, y1),
                (x2, y1),
                (x1, vy),
                (x2, vy),
                (x1, y2),
                (vx, y2),
                (x2, y2),
            ],
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum Action {
    Start,
    Right,
    Down,
    Left,
    Up,
}

pub struct SpiralIter {
    bounds: (Bound, Bound),
    position: (usize, usize),
    action: Action,
}

impl SpiralIter {
    pub fn new(major: Bound, minor: Bound) -> Self {
        Self {
            bounds: (major, minor),
            position: (major.min, minor.min),
            action: Action::Start,
        }
    }

    pub fn from_grid_layout(major: usize, minor: usize) -> Self {
        Self::new(Bound { min: 0, max: major }, Bound { min: 0, max: minor })
    }
}

impl Iterator for SpiralIter {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.bounds.0.min == self.bounds.0.max || self.bounds.1.min == self.bounds.1.max {
            return None;
        }

        match self.action {
            // Initial case
            Action::Start => {
                self.action = Action::Right;
            }

            Action::Right => {
                if self.position.0 + 1 == self.bounds.0.max {
                    self.bounds.1.min += 1;
                    self.action = Action::Down;
                    return self.next();
                } else {
                    self.position.0 += 1;
                }
            }
            Action::Down => {
                if self.position.1 + 1 == self.bounds.1.max {
                    self.bounds.0.max -= 1;
                    self.action = Action::Left;
                    return self.next();
                } else {
                    self.position.1 += 1;
                }
            }
            Action::Left => {
                if self.position.0 == self.bounds.0.min {
                    self.bounds.1.max -= 1;
                    self.action = Action::Up;
                    return self.next();
                } else {
                    self.position.0 -= 1;
                }
            }
            Action::Up => {
                if self.position.1 == self.bounds.1.min {
                    self.bounds.0.min += 1;
                    self.action = Action::Right;
                    return self.next();
                } else {
                    self.position.1 -= 1;
                }
            }
        }

        Some(self.position)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let width = self.bounds.0.max - self.bounds.0.min;
        let height = self.bounds.1.max - self.bounds.1.min;

        let remaining_zone = width * height;

        let current_gone = match self.action {
            Action::Start => 0,
            Action::Right => self.position.0 - self.bounds.0.min + 1,
            Action::Down => self.position.1 - self.bounds.1.min + 1,
            Action::Left => self.bounds.0.max - self.position.0,
            Action::Up => self.bounds.1.max - self.position.1,
        };

        let remaining = remaining_zone - current_gone;

        (remaining, Some(remaining))
    }
}

impl FusedIterator for SpiralIter {}
impl ExactSizeIterator for SpiralIter {}

pub struct RowIter {
    row: usize,
    columns: RangeInclusive<usize>,
}

impl RowIter {
    pub fn new(row: usize, columns: Bound) -> Self {
        Self {
            row,
            columns: columns.iter(),
        }
    }
}

impl Iterator for RowIter {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        self.columns.next().map(|column| (self.row, column))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.columns.size_hint()
    }
}

impl DoubleEndedIterator for RowIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.columns.next_back().map(|column| (self.row, column))
    }
}

pub struct ColumnIter {
    column: usize,
    rows: RangeInclusive<usize>,
}

impl ColumnIter {
    pub fn new(column: usize, rows: Bound) -> Self {
        Self {
            column,
            rows: rows.iter(),
        }
    }
}

impl Iterator for ColumnIter {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        self.rows.next().map(|row| (row, self.column))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.rows.size_hint()
    }
}

impl DoubleEndedIterator for ColumnIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.rows.next_back().map(|row| (row, self.column))
    }
}

pub struct RowsIter {
    rows: RangeInclusive<usize>,
    column_bounds: Bound,
}

impl Iterator for RowsIter {
    type Item = RowIter;

    fn next(&mut self) -> Option<Self::Item> {
        self.rows
            .next()
            .map(|row| RowIter::new(row, self.column_bounds))
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.rows.size_hint()
    }
}

impl DoubleEndedIterator for RowsIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.rows
            .next_back()
            .map(|row| RowIter::new(row, self.column_bounds))
    }
}

pub struct ColumnsIter {
    columns: RangeInclusive<usize>,
    row_bounds: Bound,
}

impl Iterator for ColumnsIter {
    type Item = ColumnIter;

    fn next(&mut self) -> Option<Self::Item> {
        self.columns
            .next()
            .map(|column| ColumnIter::new(column, self.row_bounds))
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.columns.size_hint()
    }
}

impl DoubleEndedIterator for ColumnsIter {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.columns
            .next_back()
            .map(|column| ColumnIter::new(column, self.row_bounds))
    }
}

impl FusedIterator for RowIter {}
impl ExactSizeIterator for RowIter {}
impl FusedIterator for ColumnIter {}
impl ExactSizeIterator for ColumnIter {}
impl FusedIterator for RowsIter {}
impl ExactSizeIterator for RowsIter {}
impl FusedIterator for ColumnsIter {}
impl ExactSizeIterator for ColumnsIter {}

impl<T> Grid for HashMap<(usize, usize), T> {
    type Item = T;

    fn bounds(&self) -> GridBounds {
        let mut width = Bound {
            min: usize::MAX,
            max: 0,
        };
        let mut height = Bound {
            min: usize::MAX,
            max: 0,
        };

        for &(x, y) in self.keys() {
            width.min = width.min.min(x);
            width.max = width.max.max(x);
            height.min = height.min.min(y);
            height.max = height.max.max(y);
        }

        GridBounds {
            x: width,
            y: height,
        }
    }

    fn get(&self, pos: (usize, usize)) -> Option<&Self::Item> {
        self.get(&pos)
    }

    fn get_mut(&mut self, pos: (usize, usize)) -> Option<&mut Self::Item> {
        self.get_mut(&pos)
    }
}

impl<T> Grid for BTreeMap<(usize, usize), T> {
    type Item = T;

    fn bounds(&self) -> GridBounds {
        let mut width = Bound {
            min: usize::MAX,
            max: 0,
        };
        let mut height = Bound {
            min: usize::MAX,
            max: 0,
        };

        for &(x, y) in self.keys() {
            width.min = width.min.min(x);
            width.max = width.max.max(x);
            height.min = height.min.min(y);
            height.max = height.max.max(y);
        }

        GridBounds {
            x: width,
            y: height,
        }
    }

    fn get(&self, pos: (usize, usize)) -> Option<&Self::Item> {
        self.get(&pos)
    }

    fn get_mut(&mut self, pos: (usize, usize)) -> Option<&mut Self::Item> {
        self.get_mut(&pos)
    }
}

impl<T> Grid for Vec<Vec<T>> {
    type Item = T;

    fn bounds(&self) -> GridBounds {
        GridBounds {
            x: Bound::new(0, self.len() - 1),
            y: Bound::new(0, self.as_slice().get(0).map_or(0, Vec::len) - 1),
        }
    }

    fn size(&self) -> (usize, usize) {
        (self.len(), if self.is_empty() { 0 } else { self[0].len() })
    }

    fn get(&self, pos: (usize, usize)) -> Option<&Self::Item> {
        self.as_slice().get(pos.0)?.as_slice().get(pos.1)
    }

    fn get_mut(&mut self, pos: (usize, usize)) -> Option<&mut Self::Item> {
        self.as_mut_slice()
            .get_mut(pos.0)?
            .as_mut_slice()
            .get_mut(pos.1)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use test_case::test_case;

    #[test_case(0,   3,  0,  3,     9;       "simple 3x3")]
    #[test_case(5,   8, 10, 13,     9;       "offset 3x3")]
    #[test_case(5, 100,  8, 36, 95*28; "large and offset")]
    fn compute_spiral_len(
        major_min: usize,
        major_max: usize,
        minor_min: usize,
        minor_max: usize,
        len: usize,
    ) {
        let mut spiral = SpiralIter::new(
            Bound {
                min: major_min,
                max: major_max,
            },
            Bound {
                min: minor_min,
                max: minor_max,
            },
        );

        assert_eq!(spiral.len(), len);

        for len in (0..len).rev() {
            assert!(spiral.next().is_some(), "spiral should not have ended");
            assert_eq!(spiral.len(), len);
        }
    }

    #[test]
    fn compute_spiral() {
        let spiraling: Vec<_> =
            SpiralIter::new(Bound { min: 1, max: 6 }, Bound { min: 1, max: 5 }).collect();

        let expected = vec![
            (1, 1),
            (2, 1),
            (3, 1),
            (4, 1),
            (5, 1),
            (5, 2),
            (5, 3),
            (5, 4),
            (4, 4),
            (3, 4),
            (2, 4),
            (1, 4),
            (1, 3),
            (1, 2),
            (2, 2),
            (3, 2),
            (4, 2),
            (4, 3),
            (3, 3),
            (2, 3),
        ];

        assert_eq!(spiraling, expected);
    }
}
