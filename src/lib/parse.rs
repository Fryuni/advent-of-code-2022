//! Parsing utilities

use color_eyre::eyre::bail;
use nom::error::VerboseError;
use nom::Parser;

/// Alias for the parse result for a &str input using verbose error reporting.
pub type Result<'a, O> = nom::IResult<&'a str, O, VerboseError<&'a str>>;

type Usize = usize;

/// Parse a `u64` from a `&str` and casts to `usize`.
pub fn usize(val: &str) -> Result<Usize> {
    nom::character::complete::u64(val).map(|(rest, val)| (rest, val as usize))
}

/// Runs a `nom::Parser` over a string and returns the
pub fn parse_all<'a, O, P>(val: &'a str, parser: P) -> color_eyre::Result<O>
where
    O: 'a,
    P: Parser<&'a str, O, VerboseError<&'a str>>,
{
    let result = nom::combinator::all_consuming(parser)
        .parse(val)
        .map(|(_, val)| val);

    match result {
        Ok(val) => Ok(val),
        Err(err) => match err {
            x @ nom::Err::Incomplete(_) => bail!("{}", x),
            nom::Err::Error(err) | nom::Err::Failure(err) => {
                let error_msg = nom::error::convert_error(val, err);

                bail!("Parsing error: {}", error_msg);
            }
        },
    }
}
