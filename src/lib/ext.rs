//! Extension of common functionalities

/// Zip two [`Result`]s into a single Result of a tuple.
pub fn zip_result<A, B, Z>(a: Result<A, Z>, b: Result<B, Z>) -> Result<(A, B), Z> {
    match (a, b) {
        (Ok(a), Ok(b)) => Ok((a, b)),
        (Err(err), _) | (_, Err(err)) => Err(err),
    }
}

/// Flatten a nested result
pub fn flat_result<T, E>(result: Result<Result<T, E>, E>) -> Result<T, E> {
    result.and_then(std::convert::identity)
}

#[cfg(test)]
mod tests {
    use test_case::case;

    #[case(Ok(1), Ok(2) => Ok::< (u8, u8), & str > ((1, 2)); "Zip ok values")]
    #[case(Err("error a"), Ok(2) => Err::< (u8, u8), & str > ("error a"); "error only first")]
    #[case(Ok(1), Err("error b") => Err::< (u8, u8), & str > ("error b"); "error only second")]
    #[case(Err("error a"), Err("error b") => Err::< (u8, u8), & str > ("error a"); "both errors")]
    pub fn zip_result<A, B, Z>(a: Result<A, Z>, b: Result<B, Z>) -> Result<(A, B), Z> {
        super::zip_result(a, b)
    }
}
