use std::cell::{Cell, RefCell};
use std::collections::BTreeMap;
use std::fmt::Formatter;
use std::rc::{Rc, Weak};

type Receiver<'a, T> = Box<dyn FnMut(&T) + 'a>;

type ReceiverMap<'a, T> = BTreeMap<usize, Receiver<'a, T>>;

/// An observer handle that allows dispatching events to multiple subscribers.
pub struct Observer<'a, Event> {
    inner: Rc<Inner<'a, Event>>,
}

struct Inner<'a, T> {
    receivers: RefCell<ReceiverMap<'a, T>>,
    counter: Cell<usize>,
}

impl<'a, T> Inner<'a, T> {
    fn new() -> Self {
        Self {
            receivers: Default::default(),
            counter: Cell::new(0),
        }
    }

    fn subscribe(&self, receiver: Receiver<'a, T>) -> usize {
        let new_token = self.get_next_token();

        self.receivers.borrow_mut().insert(new_token, receiver);

        new_token
    }

    fn unsubscribe(&self, token: usize) {
        self.receivers.borrow_mut().remove(&token);
    }

    fn dispatch(&self, event: T) {
        self.receivers
            .borrow_mut()
            .values_mut()
            .for_each(|receiver| {
                receiver(&event);
            })
    }

    fn get_next_token(&self) -> usize {
        let next_token = self.counter.get() + 1;
        self.counter.set(next_token);
        next_token
    }
}

/// A token that represents a subscription to an Observer.
/// This token can be used to cancel a subscription.
pub struct SubscriptionToken<'a, T> {
    inner: Weak<Inner<'a, T>>,
    token: usize,
}

impl<'a, T> Observer<'a, T> {
    /// Create a new empty observer.
    pub fn new() -> Self {
        Self {
            inner: Rc::new(Inner::new()),
        }
    }

    /// Subscribe to events dispatched to this observer.
    /// Every time an event is dispatched to this observer, a reference of it will be given
    /// to every subscriber.
    pub fn subscribe_boxed(&self, receiver: Receiver<'a, T>) -> SubscriptionToken<'a, T> {
        let token = self.inner.subscribe(receiver);

        SubscriptionToken {
            inner: Rc::downgrade(&self.inner),
            token,
        }
    }

    /// Subscribe to events dispatched to this observer.
    /// This method boxes the function and passed it to [`Observer::subscribe_boxed`].
    pub fn subscribe<F: FnMut(&T) + 'a>(&self, func: F) -> SubscriptionToken<'a, T> {
        self.subscribe_boxed(Box::new(func))
    }

    /// Subscribe to events that can be converted into another type.
    /// This can be used to subscribe to a single variant of an enum:
    pub fn subscribe_from<S, F>(&self, func: F) -> SubscriptionToken<'a, T>
    where
        for<'s> &'s T: TryInto<&'s S>,
        F: FnMut(&S) + 'a,
    {
        let mut func = func;
        self.subscribe(move |event| {
            if let Ok(converted) = event.try_into() {
                func(converted)
            }
        })
    }

    /// Dispatch an event to all subscribers.
    pub fn dispatch(&self, event: T) {
        self.inner.dispatch(event)
    }

    /// Dispatch an event to all subscribers from a convertible value.
    pub fn dispatch_into<S: Into<T>>(&self, event: S) {
        self.dispatch(event.into())
    }
}

impl<T> SubscriptionToken<'_, T> {
    /// Cancel the subscription that generated this token.
    pub fn cancel(self) {
        if let Some(inner) = self.inner.upgrade() {
            inner.unsubscribe(self.token)
        }
    }
}

impl<T> std::fmt::Debug for Observer<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Observer<{}> {{ {} }}",
            core::any::type_name::<T>(),
            self.inner.counter.get(),
        )
    }
}

impl<T> std::fmt::Debug for SubscriptionToken<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "SubscriptionToken<{}> {{ [opaque] }}",
            core::any::type_name::<T>(),
        )
    }
}

impl<T> Default for Observer<'_, T> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Clone, derive_more::TryInto, derive_more::From)]
    #[try_into(owned, ref)]
    enum Event {
        Int(i64),
        String(String),
    }

    impl From<&'_ str> for Event {
        fn from(value: &str) -> Self {
            Event::String(value.to_owned())
        }
    }

    #[test]
    fn simple_observer() {
        let mut value: Vec<&'static str> = Default::default();

        let observer = Observer::<&'static str>::new();

        observer.subscribe(|&message| {
            value.push(message);
        });

        observer.dispatch("foo");
        observer.dispatch("bar");
        observer.dispatch("baz");

        drop(observer);

        assert_eq!(&*value, &["foo", "bar", "baz"]);
    }

    #[test]
    fn enum_observer() {
        let mut value: Vec<i64> = Default::default();

        let observer = Observer::<Event>::new();

        observer.subscribe_from(|&message| {
            value.push(message);
        });

        observer.dispatch_into(123);
        observer.dispatch_into("foo");
        observer.dispatch_into(456);

        drop(observer);

        assert_eq!(&*value, &[123, 456]);
    }

    #[test]
    fn cancel_subscription() {
        let mut value: Vec<&'static str> = Default::default();

        let observer = Observer::<&'static str>::new();

        let subscription = observer.subscribe(|&message| {
            value.push(message);
        });

        observer.dispatch("foo");
        observer.dispatch("bar");
        subscription.cancel();
        observer.dispatch("baz");

        drop(observer);

        assert_eq!(&*value, &["foo", "bar"]);
    }

    #[test]
    fn debug_strings() {
        let observer = Observer::<String>::new();
        let subscription_token = observer.subscribe(|_| {});

        assert_eq!(
            "Observer<alloc::string::String> { 1 }",
            format!("{observer:?}"),
        );
        assert_eq!(
            "SubscriptionToken<alloc::string::String> { [opaque] }",
            format!("{subscription_token:?}"),
        );
    }
}
