use std::collections::BTreeMap;
use std::fmt::Formatter;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::mpsc;
use std::sync::{Arc, RwLock, Weak};
use std::thread;

type Receiver<T> = Box<dyn Fn(&T) + Send + Sync + 'static>;

type ReceiverMap<T> = RwLock<BTreeMap<usize, Receiver<T>>>;

/// An observer handle that allows dispatching events to multiple subscribers.
pub struct Observer<Event> {
    inner: Arc<Inner<Event>>,
}

struct Inner<T> {
    receivers: ReceiverMap<T>,
    counter: AtomicUsize,
}

impl<T> Inner<T> {
    fn new() -> Self {
        Self {
            receivers: Default::default(),
            counter: AtomicUsize::new(0),
        }
    }

    fn subscribe(&self, receiver: Receiver<T>) -> usize {
        let new_token = self.get_next_token();

        self.receivers
            .write()
            .expect("lock should not be poisoned")
            .insert(new_token, receiver);

        new_token
    }

    fn unsubscribe(&self, token: usize) {
        self.receivers
            .write()
            .expect("lock should not be poisoned")
            .remove(&token);
    }

    fn dispatch(&self, event: T) {
        self.receivers
            .read()
            .expect("lock should not be poisoned")
            .values()
            .for_each(|receiver| {
                receiver(&event);
            })
    }

    fn get_next_token(&self) -> usize {
        self.counter.fetch_add(1, Ordering::SeqCst)
    }
}

/// A token that represents a subscription to an Observer.
/// This token can be used to cancel a subscription.
pub struct SubscriptionToken<T> {
    inner: Weak<Inner<T>>,
    token: usize,
}

impl<T> Observer<T> {
    /// Create a new empty observer.
    pub fn new() -> Self {
        Self {
            inner: Arc::new(Inner::new()),
        }
    }

    /// Subscribe to events dispatched to this observer.
    /// Every time an event is dispatched to this observer, a reference of it will be given
    /// to every subscriber.
    pub fn subscribe_boxed(&self, receiver: Receiver<T>) -> SubscriptionToken<T> {
        let token = self.inner.subscribe(receiver);

        SubscriptionToken {
            inner: Arc::downgrade(&self.inner),
            token,
        }
    }

    /// Subscribe to events dispatched to this observer.
    /// This method boxes the function and passed it to [`Observer::subscribe_boxed`].
    pub fn subscribe<F: Fn(&T) + Send + Sync + 'static>(&self, func: F) -> SubscriptionToken<T> {
        self.subscribe_boxed(Box::new(func))
    }

    /// Subscribe to events that can be converted into another type.
    /// This can be used to subscribe to a single variant of an enum:
    pub fn subscribe_from<S, F>(&self, func: F) -> SubscriptionToken<T>
    where
        for<'a> &'a T: TryInto<&'a S>,
        F: Fn(&S) + Send + Sync + 'static,
    {
        self.subscribe(move |event| {
            if let Ok(converted) = event.try_into() {
                func(converted)
            }
        })
    }

    /// Dispatch an event to all subscribers.
    pub fn dispatch(&self, event: T) {
        self.inner.dispatch(event)
    }

    /// Dispatch an event to all subscribers from a convertible value.
    pub fn dispatch_into<S: Into<T>>(&self, event: S) {
        self.dispatch(event.into())
    }

    /// Create a new channel with the given capacity and start a new thread to dispatch the
    /// events send into this channel to this observer.
    /// Events send to the same channel, even if through different senders, are processed sequentially,
    /// calling [`Observer::dispatch`] for each event received.
    ///
    /// The semantics of capacity are the same as [`mpsc::sync_channel`].
    ///
    /// ### Panics
    /// If a receiver panics during an event dispatch, the thread will stop and this channel will
    /// be dropped. Unprocessed events will be discarded.
    pub fn background(&self, capacity: usize) -> mpsc::SyncSender<T>
    where
        T: Send + 'static,
    {
        let (sender, receiver) = mpsc::sync_channel(capacity);

        let shadow = self.shared_clone();

        thread::spawn(move || {
            while let Ok(event) = receiver.recv() {
                shadow.dispatch(event);
            }
        });

        sender
    }

    fn shared_clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }
}

impl<T> SubscriptionToken<T> {
    /// Cancel the subscription that generated this token.
    pub fn cancel(self) {
        if let Some(inner) = self.inner.upgrade() {
            inner.unsubscribe(self.token)
        }
    }
}

impl<T> std::fmt::Debug for Observer<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Observer<{}> {{ {} }}",
            core::any::type_name::<T>(),
            self.inner.counter.load(Ordering::Relaxed),
        )
    }
}

impl<T> std::fmt::Debug for SubscriptionToken<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "SubscriptionToken<{}> {{ [opaque] }}",
            core::any::type_name::<T>(),
        )
    }
}

impl<T> Default for Observer<T> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Mutex;

    use super::*;

    #[derive(Clone, derive_more::TryInto, derive_more::From)]
    #[try_into(owned, ref)]
    enum Event {
        Int(i64),
        String(String),
    }

    impl From<&'_ str> for Event {
        fn from(value: &str) -> Self {
            Event::String(value.to_owned())
        }
    }

    #[test]
    fn simple_observer() {
        let observer = Observer::<&'static str>::new();
        let value: Arc<Mutex<Vec<&'static str>>> = Default::default();

        {
            let value = Arc::clone(&value);
            observer.subscribe(move |&message| {
                value.lock().unwrap().push(message);
            });
        }

        observer.dispatch("foo");
        observer.dispatch("bar");
        observer.dispatch("baz");

        assert_eq!(&*value.lock().unwrap(), &["foo", "bar", "baz"]);
    }

    #[test]
    fn enum_observer() {
        let observer = Observer::<Event>::new();
        let value: Arc<Mutex<Vec<i64>>> = Default::default();

        {
            let value = Arc::clone(&value);
            observer.subscribe_from(move |&message| {
                value.lock().unwrap().push(message);
            });
        }

        observer.dispatch_into(123);
        observer.dispatch_into("foo");
        observer.dispatch_into(456);

        assert_eq!(&*value.lock().unwrap(), &[123, 456]);
    }

    #[test]
    fn cancel_subscription() {
        let observer = Observer::<&'static str>::new();
        let value: Arc<Mutex<Vec<&'static str>>> = Default::default();

        let subscription = {
            let value = Arc::clone(&value);
            observer.subscribe(move |&message| {
                value.lock().unwrap().push(message);
            })
        };

        observer.dispatch("foo");
        observer.dispatch("bar");
        subscription.cancel();
        observer.dispatch("baz");

        assert_eq!(&*value.lock().unwrap(), &["foo", "bar"]);
    }

    #[test]
    fn debug_strings() {
        let observer = Observer::<String>::new();
        let subscription_token = observer.subscribe(|_| {});

        assert_eq!(
            "Observer<alloc::string::String> { 1 }",
            format!("{observer:?}"),
        );
        assert_eq!(
            "SubscriptionToken<alloc::string::String> { [opaque] }",
            format!("{subscription_token:?}"),
        );
    }
}
