//! Implementation of a generic observer

/// Thread-safe observer
pub mod sync;

/// Lightweight non-thread-safe observer
mod local;

pub use local::{Observer, SubscriptionToken};
