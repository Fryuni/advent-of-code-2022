pub trait Solution {
    type Output;

    fn part_one(&self, input: String) -> color_eyre::Result<Self::Output>;

    fn part_two(&self, input: String) -> color_eyre::Result<Self::Output>;
}

pub type SolutionRef = &'static (dyn Solution<Output = String> + Sync);

pub struct DisplaySolution<S>(pub S)
where
    S: Solution,
    S::Output: ToString;

impl<S> Solution for DisplaySolution<S>
where
    S: Solution,
    S::Output: ToString,
{
    type Output = String;

    fn part_one(&self, input: String) -> color_eyre::Result<Self::Output> {
        self.0.part_one(input).map(|val| val.to_string())
    }

    fn part_two(&self, input: String) -> color_eyre::Result<Self::Output> {
        self.0.part_two(input).map(|val| val.to_string())
    }
}

pub type YearSolutions = [SolutionRef; 25];

static REGISTRY: [YearSolutions; 4] = [
    crate::y2019::SOLUTIONS,
    crate::y2020::SOLUTIONS,
    crate::y2021::SOLUTIONS,
    crate::y2022::SOLUTIONS,
];

pub(super) fn get_solution(year: u16, day: u8) -> SolutionRef {
    debug_assert!((2019..=2022).contains(&year));
    debug_assert!((1..=25).contains(&day));

    REGISTRY[year as usize - 2019][day as usize - 1]
}
