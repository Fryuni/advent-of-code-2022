pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, _input: String) -> color_eyre::Result<usize> {
        color_eyre::eyre::bail!("Not implemented")
    }

    fn part_two(&self, _input: String) -> color_eyre::Result<usize> {
        color_eyre::eyre::bail!("Not implemented")
    }
}
