use clap::builder::TypedValueParser;
use color_eyre::eyre::eyre;
use itertools::Itertools;
use nom::Parser;
use std::fmt;
use std::fmt::{Formatter, Write};

#[derive(Copy, Clone, Debug, derive_more::From)]
struct Crate(char);

impl Crate {
    fn parse(text: &str) -> aoc::parse::Result<Self> {
        nom::sequence::delimited(
            nom::character::complete::char('['),
            nom::character::complete::anychar,
            nom::character::complete::char(']'),
        )
        .map(Self::from)
        .parse(text)
    }
}

impl fmt::Display for Crate {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "[{}]", self.0)
    }
}

#[derive(Clone)]
struct CrateStacks {
    stacks: Vec<Vec<Crate>>,
}

impl CrateStacks {
    fn parse(text: &str) -> aoc::parse::Result<Self> {
        nom::sequence::terminated(
            nom::multi::separated_list0(nom::character::complete::newline, Self::parse_line),
            nom::sequence::terminated(
                nom::character::complete::newline,
                nom::character::complete::not_line_ending,
            ),
        )
        .map(|matrix| {
            let mut stacks = vec![Vec::new(); matrix.last().map(Vec::len).unwrap_or_default()];

            for row in matrix.into_iter().rev() {
                for (idx, crate_) in row.into_iter().enumerate() {
                    if let Some(crate_) = crate_ {
                        stacks[idx].push(crate_);
                    }
                }
            }

            Self { stacks }
        })
        .parse(text)
    }

    fn parse_line(line: &str) -> aoc::parse::Result<Vec<Option<Crate>>> {
        nom::multi::separated_list1(
            nom::character::complete::char(' '),
            nom::branch::alt((
                nom::multi::count(nom::character::complete::char(' '), 3).map(|_| None),
                Crate::parse.map(Some),
            )),
        )(line)
    }
}

impl fmt::Display for CrateStacks {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let stack_top = self.stacks.iter().map(Vec::len).max().unwrap_or_default();
        let stack_count = self.stacks.len();

        for height in (0..stack_top).rev() {
            Itertools::intersperse_with(
                (0..stack_count)
                    .map(|stack| self.stacks[stack].get(height))
                    .map(Some),
                || None,
            )
            .try_for_each(|val| -> fmt::Result {
                match val {
                    None => f.write_char(' ')?,
                    Some(None) => f.write_str("   ")?,
                    Some(Some(crate_)) => write!(f, "{}", crate_)?,
                };

                Ok(())
            })?;

            f.write_char('\n')?;
        }

        Ok(())
    }
}

impl fmt::Debug for CrateStacks {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

#[derive(Clone, Debug)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

impl Instruction {
    fn parse(text: &str) -> aoc::parse::Result<Self> {
        nom::sequence::tuple((
            nom::sequence::preceded(nom::bytes::complete::tag("move "), aoc::parse::usize),
            nom::sequence::preceded(nom::bytes::complete::tag(" from "), aoc::parse::usize),
            nom::sequence::preceded(nom::bytes::complete::tag(" to "), aoc::parse::usize),
        ))
        .map(|(count, from, to)| Self { count, from, to })
        .parse(text)
    }

    fn apply_individually(&self, state: &mut CrateStacks) -> color_eyre::Result<()> {
        for _ in 0..self.count {
            let val = state
                .stacks
                .get_mut(self.from - 1)
                .ok_or(eyre!("from stack out of bounds"))?
                .pop()
                .ok_or(eyre!("must not move from empty stack"))?;

            state
                .stacks
                .get_mut(self.to - 1)
                .ok_or(eyre!("to stack out of bounds"))?
                .push(val);
        }

        Ok(())
    }

    fn apply_bulk(&self, state: &mut CrateStacks) -> color_eyre::Result<()> {
        let (source, target) = {
            if self.from > state.stacks.len() {
                color_eyre::eyre::bail!("from stack out of bounds");
            }

            if self.to > state.stacks.len() {
                color_eyre::eyre::bail!(
                    "target stack out of bounds, targeting {} having {}",
                    self.to,
                    state.stacks.len()
                );
            }

            let (lower, upper) = state
                .stacks
                .split_at_mut(usize::max(self.from, self.to) - 1);

            if self.from < self.to {
                (&mut lower[self.from - 1], &mut upper[0])
            } else {
                (&mut upper[0], &mut lower[self.to - 1])
            }
        };

        let cut_point = source.len() - self.count;

        target.extend_from_slice(source.get(cut_point..).ok_or(eyre!("cut out of bounds"))?);

        source.truncate(cut_point);

        Ok(())
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "move {} from {} to {}", self.count, self.from, self.to)
    }
}

#[derive(Clone, Debug)]
struct Input {
    state: CrateStacks,
    instructions: Vec<Instruction>,
}

impl Input {
    fn parse(text: &str) -> aoc::parse::Result<Self> {
        nom::sequence::terminated(
            nom::sequence::separated_pair(
                CrateStacks::parse,
                nom::multi::count(nom::character::complete::line_ending, 2),
                nom::multi::separated_list1(nom::character::complete::newline, Instruction::parse),
            ),
            nom::multi::many0(nom::character::complete::line_ending),
        )
        .map(|(stack, instructions)| Self {
            state: stack,
            instructions,
        })
        .parse(text)
    }
}

impl fmt::Display for Input {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", self.state)?;

        for instruction in &self.instructions {
            writeln!(f, "{}", instruction)?;
        }

        Ok(())
    }
}

pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = String;
    fn part_one(&self, input: String) -> color_eyre::Result<String> {
        let mut input = aoc::parse::parse_all(&input, Input::parse)?;

        for instruction in input.instructions.drain(..) {
            instruction.apply_individually(&mut input.state)?;
        }

        Ok(input
            .state
            .stacks
            .into_iter()
            .flat_map(|stack| stack.into_iter().next_back())
            .map(|c| c.0)
            .collect())
    }

    fn part_two(&self, input: String) -> color_eyre::Result<String> {
        let mut input = aoc::parse::parse_all(&input, Input::parse)?;

        for instruction in input.instructions.drain(..) {
            instruction.apply_bulk(&mut input.state)?;
        }

        Ok(input
            .state
            .stacks
            .into_iter()
            .flat_map(|stack| stack.into_iter().next_back())
            .map(|c| c.0)
            .collect())
    }
}
