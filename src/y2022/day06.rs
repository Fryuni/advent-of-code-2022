use itertools::Itertools;
use nom::InputIter;

fn find_marker(marker_size: usize, message: &str) -> usize {
    let mut pos = 0;

    while let Some(window) = message.get(pos..pos + marker_size) {
        pos += match window
            .bytes()
            .enumerate()
            .rposition(|(cut, byte)| window[cut + 1..].as_bytes().contains(&byte))
        {
            None => return pos + marker_size,
            Some(off) => off + 1,
        }
    }

    pos + marker_size
}

pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = String;
    fn part_one(&self, input: String) -> color_eyre::Result<Self::Output> {
        Ok(input
            .lines()
            .map(|line| find_marker(4, line))
            .map(|pos| pos.to_string())
            .join("\n"))
    }

    fn part_two(&self, input: String) -> color_eyre::Result<Self::Output> {
        Ok(input
            .lines()
            .map(|line| find_marker(14, line))
            .map(|pos| pos.to_string())
            .join("\n"))
    }
}
