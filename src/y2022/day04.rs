use aoc::parse;
use color_eyre::Result;
use std::collections::Bound;

pub struct Solution;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct Assignment(usize, usize);

impl Assignment {
    fn parse(val: &str) -> parse::Result<Self> {
        nom::combinator::map(
            nom::sequence::separated_pair(
                parse::usize,
                nom::bytes::complete::tag("-"),
                parse::usize,
            ),
            |(start, end)| Self(start, end),
        )(val)
    }

    fn into_range(self) -> std::ops::RangeInclusive<usize> {
        self.0..=self.1
    }

    fn contains(self, other: Self) -> bool {
        let range = self.into_range();

        range.contains(&other.0) && range.contains(&other.1)
    }

    fn overlap(self, other: Self) -> bool {
        let range = self.into_range();

        range.contains(&other.0) || range.contains(&other.1)
    }
}

impl std::ops::RangeBounds<usize> for Assignment {
    fn start_bound(&self) -> Bound<&usize> {
        Bound::Included(&self.0)
    }

    fn end_bound(&self) -> Bound<&usize> {
        Bound::Included(&self.1)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct ElvenPair(Assignment, Assignment);

impl ElvenPair {
    fn parse(val: &str) -> parse::Result<Self> {
        nom::combinator::map(
            nom::sequence::separated_pair(
                Assignment::parse,
                nom::bytes::complete::tag(","),
                Assignment::parse,
            ),
            |(start, end)| Self(start, end),
        )(val)
    }

    fn parse_all_line(val: &str) -> Result<Vec<Self>> {
        parse::parse_all(
            val.trim(),
            nom::multi::separated_list0(nom::character::complete::newline, Self::parse),
        )
    }
}

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> Result<usize> {
        ElvenPair::parse_all_line(input.as_str()).map(|pairs| {
            pairs
                .into_iter()
                .filter(|&ElvenPair(left, right)| left.contains(right) || right.contains(left))
                .count()
        })
    }

    fn part_two(&self, input: String) -> Result<usize> {
        ElvenPair::parse_all_line(input.as_str()).map(|pairs| {
            pairs
                .into_iter()
                .filter(|&ElvenPair(left, right)| left.overlap(right) || right.overlap(left))
                .count()
        })
    }
}

#[cfg(test)]
mod test {
    use crate::registry::Solution;
    use test_case::test_case;

    use super::*;

    #[test_case("1-3,6-8" => Ok(("", ElvenPair(Assignment(1, 3), Assignment(6, 8)))))]
    fn parse_line(val: &str) -> parse::Result<ElvenPair> {
        ElvenPair::parse(val)
    }

    static SAMPLE: &'static str = "\
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

    #[test_case(SAMPLE => 2)]
    fn part_one(input: &str) -> usize {
        Solution.part_one(input.to_string()).unwrap()
    }

    #[test_case(SAMPLE => 4)]
    fn part_two(input: &str) -> usize {
        Solution.part_two(input.to_string()).unwrap()
    }
}
