use color_eyre::eyre::eyre;
use std::collections::{BTreeMap, VecDeque};
use std::fmt;
use std::fmt::{Debug, Formatter, Write};
use std::str::FromStr;

use either::Either;
use itertools::Itertools;
use log::debug;

use aoc::grid::{Grid, GridBounds};

#[derive(Clone)]
struct Forest {
    grid: Vec<Vec<u8>>,
}

impl FromStr for Forest {
    type Err = core::convert::Infallible;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            grid: s
                .lines()
                .map(|line| line.bytes().map(|b| b - b'0').collect())
                .collect(),
        })
    }
}

impl Grid for Forest {
    type Item = u8;

    fn bounds(&self) -> GridBounds {
        Grid::bounds(&self.grid)
    }

    fn get(&self, pos: (usize, usize)) -> Option<&Self::Item> {
        Grid::get(&self.grid, pos)
    }

    fn get_mut(&mut self, pos: (usize, usize)) -> Option<&mut Self::Item> {
        Grid::get_mut(&mut self.grid, pos)
    }
}

impl fmt::Display for Forest {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        const HEADER: &str = "Forest";

        let width = self.size().1 + 2;

        writeln!(f, " {:-^width$}", HEADER)?;

        let mut buf = String::with_capacity(width - 2);

        for line in &self.grid {
            buf.truncate(0);

            f.write_str("| ")?;

            buf.extend(line.iter().map(|&b| char::from(b + b'0')));
            f.write_str(&buf)?;

            f.write_str(" |\n")?;
        }

        write!(f, " {:-^width$}", "")?;

        Ok(())
    }
}

impl Forest {
    fn compute_linear_visibility(&self) -> BTreeMap<(usize, usize), bool> {
        let (max_x, max_y) = self.size();

        let mut visibility = BTreeMap::from_iter(
            (0..=max_x)
                .cartesian_product(0..=max_y)
                .map(|(x, y)| ((x, y), false)),
        );

        let bounds = self.bounds();

        bounds
            .iter_rows()
            .map(Either::Left)
            .chain(bounds.iter_rows().map(|row| Either::Right(row.rev())))
            .map(Either::Left)
            .chain(
                bounds
                    .iter_columns()
                    .map(Either::Left)
                    .chain(
                        bounds
                            .iter_columns()
                            .map(|column| Either::Right(column.rev())),
                    )
                    .map(Either::Right),
            )
            .filter_map(|mut line| Some((line.next()?, line)))
            .for_each(|(first, line)| {
                visibility.insert(first, true);

                static BOUND_EXPECT: &str = "every tree in bounds should have a value";

                let mut max = self.get(first).copied().expect(BOUND_EXPECT);

                debug!("First pos: {first:?} => {max}");

                for pos in line {
                    let value = self.get(pos).copied().expect(BOUND_EXPECT);

                    if value > max {
                        max = value;
                        visibility.insert(pos, true);
                        debug!("VISIBLE position: {pos:?} => {value}");
                    } else {
                        // debug!("NON VISIBLE position: {pos:?} => {value}");
                        // visibility.insert(pos, false);
                    }
                }
            });

        visibility
    }

    fn compute_scores(&self) -> BTreeMap<(usize, usize), usize> {
        let mut scores: BTreeMap<(usize, usize), usize> = BTreeMap::new();

        let bounds = self.bounds();

        let mut buffer: Vec<((usize, usize), u8)> = Vec::new();

        bounds
            .iter_rows()
            .map(Either::Left)
            .chain(bounds.iter_rows().map(|row| Either::Right(row.rev())))
            .map(Either::Left)
            .chain(
                bounds
                    .iter_columns()
                    .map(Either::Left)
                    .chain(
                        bounds
                            .iter_columns()
                            .map(|column| Either::Right(column.rev())),
                    )
                    .map(Either::Right),
            )
            // .filter_map(|mut line| Some((line.next()?, line)))
            .for_each(|line| {
                static BOUND_EXPECT: &str = "every tree in bounds should have a value";

                buffer.truncate(0);
                buffer.extend(line.map(|pos| (pos, self.get(pos).copied().expect(BOUND_EXPECT))));

                if let Some(&(pos, _)) = buffer.first() {
                    scores.insert(pos, 0);
                }

                let last_pos = buffer.last().expect(BOUND_EXPECT).0;

                for (idx, &(pos, val)) in buffer.iter().enumerate() {
                    let distance = buffer[idx + 1..]
                        .iter()
                        .find_position(|(_, other)| *other >= val)
                        .map(|(other_idx, _)| other_idx + 1)
                        // Visible up till the last tree
                        .unwrap_or(buffer.len() - idx - 1);

                    debug!("{pos:?} sight of {distance} towards {last_pos:?}");

                    *scores.entry(pos).or_insert(1) *= distance;
                }
            });

        scores
    }
}

pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> color_eyre::Result<usize> {
        let forest: Forest = input.parse()?;

        Ok(forest
            .compute_linear_visibility()
            .into_values()
            .filter(|x| *x)
            .count())
    }

    fn part_two(&self, input: String) -> color_eyre::Result<usize> {
        let forest: Forest = input.parse()?;

        forest
            .compute_scores()
            .into_values()
            .max()
            .ok_or(eyre!("No visible tree"))
    }
}
