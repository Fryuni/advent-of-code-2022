use crate::registry::{DisplaySolution, YearSolutions};

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

pub static SOLUTIONS: YearSolutions = [
    &DisplaySolution(day01::Solution),
    &DisplaySolution(day02::Solution),
    &DisplaySolution(day03::Solution),
    &DisplaySolution(day04::Solution),
    &DisplaySolution(day05::Solution),
    &DisplaySolution(day06::Solution),
    &DisplaySolution(day07::Solution),
    &DisplaySolution(day08::Solution),
    &DisplaySolution(day09::Solution),
    &DisplaySolution(day10::Solution),
    &DisplaySolution(day11::Solution),
    &DisplaySolution(day12::Solution),
    &DisplaySolution(day13::Solution),
    &DisplaySolution(day14::Solution),
    &DisplaySolution(day15::Solution),
    &DisplaySolution(day16::Solution),
    &DisplaySolution(day17::Solution),
    &DisplaySolution(day18::Solution),
    &DisplaySolution(day19::Solution),
    &DisplaySolution(day20::Solution),
    &DisplaySolution(day21::Solution),
    &DisplaySolution(day22::Solution),
    &DisplaySolution(day23::Solution),
    &DisplaySolution(day24::Solution),
    &DisplaySolution(day25::Solution),
];
