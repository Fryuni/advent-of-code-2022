pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> color_eyre::Result<usize> {
        Ok(input
            .lines()
            .map(|line| line.parse::<usize>().ok())
            .fold((0, 0), |(best, current), v| match v {
                Some(v) => (best.max(current + v), current + v),
                None => (best, 0),
            })
            .0)
    }

    fn part_two(&self, input: String) -> color_eyre::Result<usize> {
        let mut calories = input.lines().map(|line| line.parse::<usize>().ok()).fold(
            vec![0],
            |mut list, value| {
                match value {
                    Some(value) => *list.last_mut().unwrap() += value,
                    None => list.push(0),
                };

                list
            },
        );
        calories.sort_unstable();

        Ok(calories.into_iter().rev().take(3).sum())
    }
}
