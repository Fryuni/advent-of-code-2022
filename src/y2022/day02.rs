use aoc::ext::zip_result;
use color_eyre::eyre::{bail, eyre, Context};
use color_eyre::{eyre, Result};
use itertools::Itertools;

pub struct Solution;

#[derive(Debug, Copy, Clone)]
enum Move {
    Rock,
    Paper,
    Scissors,
}

impl Move {
    fn win_against(other: Self) -> Self {
        match other {
            Self::Rock => Self::Paper,
            Self::Paper => Self::Scissors,
            Self::Scissors => Self::Rock,
        }
    }

    fn loses_against(other: Self) -> Self {
        match other {
            Self::Rock => Self::Scissors,
            Self::Paper => Self::Rock,
            Self::Scissors => Self::Paper,
        }
    }

    fn from_opponent(value: &str) -> Result<Self> {
        Ok(match value {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,
            _ => bail!("Invalid move code {}", value),
        })
    }

    fn from_me(value: &str) -> Result<Self> {
        Ok(match value {
            "X" => Self::Rock,
            "Y" => Self::Paper,
            "Z" => Self::Scissors,
            _ => bail!("Invalid move code {}", value),
        })
    }

    fn from_result(result: &str, other: Self) -> Result<Self> {
        Ok(match result {
            "X" => Self::loses_against(other),
            "Y" => other,
            "Z" => Self::win_against(other),
            _ => bail!("Invalid result code {}", result),
        })
    }

    fn shape_score(self) -> usize {
        match self {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissors => 3,
        }
    }

    fn score_against(self, opponent: Self) -> usize {
        use Move::*;

        self.shape_score()
            + match (self, opponent) {
                (Rock, Paper) | (Paper, Scissors) | (Scissors, Rock) => 0,
                (Paper, Rock) | (Scissors, Paper) | (Rock, Scissors) => 6,
                _ => 3,
            }
    }
}

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> Result<usize> {
        input
            .lines()
            .map(|line| line.split_once(' ').ok_or(eyre!("Invalid line")))
            .map_ok(|(opponent, me)| {
                zip_result(
                    Move::from_opponent(opponent).wrap_err("Failed to parse opponent move"),
                    Move::from_me(me).wrap_err("Failed to parse my move"),
                )
            })
            .flatten_ok()
            .map_ok(|(opponent, me)| me.score_against(opponent))
            .sum()
    }

    fn part_two(&self, input: String) -> Result<usize> {
        input
            .lines()
            .map(|line| line.split_once(' ').ok_or(eyre!("Invalid line")))
            .map_ok(|(opponent, me)| -> Result<(Move, Move)> {
                let op_move =
                    Move::from_opponent(opponent).wrap_err("Failed to parse opponent move")?;
                let my_move = Move::from_result(me, op_move).wrap_err("Failed to parse my move")?;

                Ok((op_move, my_move))
            })
            .flatten_ok()
            .map_ok(|(opponent, me)| me.score_against(opponent))
            .sum()
    }
}
