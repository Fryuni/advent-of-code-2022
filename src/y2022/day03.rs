use color_eyre::eyre::{bail, eyre};
use color_eyre::{eyre, Result};
use itertools::Itertools;

pub struct Solution;

fn char_priority(c: char) -> Result<usize> {
    Ok(match c {
        'a'..='z' => c as usize - 'a' as usize + 1,
        'A'..='Z' => c as usize - 'A' as usize + 27,
        _ => bail!("Unrecognized char: {}", c),
    })
}

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> Result<usize> {
        input
            .lines()
            .map(|line| line.split_at(line.len() / 2))
            .flat_map(|(left, right)| {
                left.chars()
                    .unique()
                    .filter(|&b| right.contains(b))
                    .map(char_priority)
            })
            .sum()
    }

    fn part_two(&self, input: String) -> Result<usize> {
        input
            .lines()
            .array_chunks::<3>()
            .map(|[a, b, c]| {
                a.chars()
                    .find(|&ch| b.contains(ch) && c.contains(ch))
                    .ok_or(eyre!("Invalid rucksack triple"))
                    .and_then(char_priority)
            })
            .sum()
    }
}
