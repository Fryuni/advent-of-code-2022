mod command_log {
    use nom::Parser;

    #[derive(Debug, Clone)]
    pub enum Command {
        Cd { target: String },
        Ls,
    }

    impl Command {
        fn parse(text: &str) -> aoc::parse::Result<Self> {
            nom::branch::alt((
                nom::bytes::complete::tag("ls").map(|_| Self::Ls),
                nom::sequence::preceded(
                    nom::bytes::complete::tag("cd "),
                    nom::character::complete::not_line_ending,
                )
                .map(|target: &str| Self::Cd {
                    target: target.to_string(),
                }),
            ))
            .parse(text)
        }
    }

    #[derive(Debug, Clone)]
    pub enum Listing {
        Dir(String),
        File { name: String, size: usize },
    }

    impl Listing {
        fn parse(text: &str) -> aoc::parse::Result<Self> {
            nom::branch::alt((
                nom::sequence::preceded(
                    nom::bytes::complete::tag("dir "),
                    nom::character::complete::not_line_ending,
                )
                .map(|name: &str| Self::Dir(name.to_string())),
                nom::sequence::separated_pair(
                    aoc::parse::usize,
                    nom::character::complete::char(' '),
                    nom::character::complete::not_line_ending,
                )
                .map(|(size, name)| Self::File {
                    size,
                    name: name.to_string(),
                }),
            ))
            .parse(text)
        }
    }

    #[derive(Debug, Clone)]
    pub enum Line {
        Command(Command),
        Listing(Listing),
    }

    impl Line {
        fn parse(text: &str) -> aoc::parse::Result<Self> {
            nom::branch::alt((
                nom::sequence::preceded(nom::bytes::complete::tag("$ "), Command::parse)
                    .map(Self::Command),
                Listing::parse.map(Self::Listing),
            ))
            .parse(text)
        }
    }

    pub fn parse(text: &str) -> aoc::parse::Result<Vec<Line>> {
        nom::multi::separated_list1(nom::character::complete::line_ending, Line::parse).parse(text)
    }
}

mod state {
    use super::command_log::{Line, Listing};
    use crate::y2022::day07::command_log::Command;
    use itertools::Itertools;
    use std::collections::{BTreeMap, HashMap};
    use std::io::Read;
    use std::path::PathBuf;

    #[derive(Default, Debug, Clone)]
    pub struct FSTree(BTreeMap<String, Entry>);

    #[derive(Debug, Clone)]
    pub enum Entry {
        Dir(FSTree),
        File { size: usize },
    }

    impl FromIterator<Listing> for FSTree {
        fn from_iter<T: IntoIterator<Item = Listing>>(log: T) -> Self {
            Self(
                log.into_iter()
                    .map(|item| match item {
                        Listing::Dir(name) => (name, Entry::Dir(Self::default())),
                        Listing::File { name, size } => (name, Entry::File { size }),
                    })
                    .collect(),
            )
        }
    }

    impl FromIterator<Line> for FSTree {
        fn from_iter<T: IntoIterator<Item = Line>>(log: T) -> Self {
            let mut top_tree = Self::default();

            let mut base_iter = log.into_iter().peekable();

            let mut cwd: Vec<String> = Vec::new();

            while let Some(Line::Command(cmd)) = base_iter.next() {
                match cmd {
                    Command::Cd { target } => match target.as_str() {
                        ".." => {
                            cwd.pop();
                        }
                        "/" => {
                            cwd.truncate(0);
                        }
                        _ => {
                            cwd.push(target);
                        }
                    },
                    Command::Ls => top_tree.absorb_at(
                        &cwd,
                        base_iter
                            .by_ref()
                            .peeking_take_while(|line| matches!(line, Line::Listing(..)))
                            .filter_map(|line| match line {
                                Line::Command(..) => None,
                                Line::Listing(ls) => Some(ls),
                            }),
                    ),
                }
            }

            top_tree
        }
    }

    impl FSTree {
        fn absorb_at(&mut self, path: &[String], listing_log: impl IntoIterator<Item = Listing>) {
            if let Some((head, rest)) = path.split_first() {
                if let Some(Entry::Dir(sub_tree)) = self.0.get_mut(head) {
                    return sub_tree.absorb_at(rest, listing_log);
                }
            }

            *self = listing_log.into_iter().collect();
        }

        pub fn size(&self) -> usize {
            self.0.values().map(Entry::size).sum()
        }

        pub fn walk<'a>(&'a self, mut f: impl FnMut((&'a str, &'a Entry))) {
            for (key, entry) in self.0.iter() {
                if let Entry::Dir(dir) = entry {
                    dir.walk(&mut f);
                }

                f((key, entry))
            }
        }

        pub fn walk_iter(&self) -> impl Iterator<Item = (&str, &Entry)> {
            self.0
                .iter()
                .flat_map(|(key, entry)| -> Box<dyn Iterator<Item = _>> {
                    match entry {
                        Entry::Dir(dir) => Box::new(
                            dir.walk_iter()
                                .chain(core::iter::once((key.as_str(), entry))),
                        ),
                        Entry::File { .. } => Box::new(core::iter::once((key.as_str(), entry))),
                    }
                })
        }
    }

    impl Entry {
        pub fn size(&self) -> usize {
            match self {
                Entry::Dir(dir) => dir.size(),
                &Entry::File { size } => size,
            }
        }
    }
}

pub struct Solution;

impl crate::registry::Solution for Solution {
    type Output = usize;
    fn part_one(&self, input: String) -> color_eyre::Result<usize> {
        let log = aoc::parse::parse_all(&input, command_log::parse)?;

        let fs: state::FSTree = log.into_iter().collect();

        Ok(fs
            .walk_iter()
            .map(|(_, entry)| entry)
            .filter(|&entry| matches!(entry, state::Entry::Dir(..)))
            .map(state::Entry::size)
            .filter(|&size| size <= 100_000)
            .sum())
    }

    fn part_two(&self, input: String) -> color_eyre::Result<usize> {
        let log = aoc::parse::parse_all(&input, command_log::parse)?;

        let fs: state::FSTree = log.into_iter().collect();

        let total = fs.size();
        let unused = 70_000_000 - total;
        let target_deletion = 30_000_000 - unused;

        fs.walk_iter()
            .map(|(_, entry)| entry)
            .filter(|&entry| matches!(entry, state::Entry::Dir(..)))
            .map(state::Entry::size)
            .filter(|&size| size >= target_deletion)
            .min()
            .ok_or(color_eyre::eyre::eyre!("no directory is big enough"))
    }
}
